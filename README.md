# HTML základy

HTML není vůbec složité, ba naopak! Ze všech jazyků, které se v počítačovém světě používají, ke jedním z nejjednodušších.

Nejedná se o programovací jazyk, ale jazyk značkovací. Znamená to, že se pomocí něj nedají psát programy, ale slouží na strukturování textu. Strukturování textu je operace, kterou pravděpodobně znáte z programu Microsoft Word.

Strukturovaný text v HTML umí zobrazit internetové prohlížeče, ve kterých je jazyk HTML nejčastěji prezentován.

## Tag
Základem veškerého značkování je tzv. “tag”. Tag se skládá z počáteční a koncové značky, mezi nimiž je umístěn text.

Takový tag vypadá například takto:

```
<p>Volně nuly, jakékoli ji duhový obývají, si myslet ke doprovázet plné fázi dávných permanentky osobního já že. K drah normálem kroky ať mizí zoologie vzrůstá horka demence odlišné k 057 metry narozen okrajové šperky pluli k aby stejná.</p>
```

Právě jsme ukázali tag `<p>` (p jako pargraph, což v angličtině znamená odstavec). Tag `<p>` je párový, takže někde začíná (`<p>`) a někde končí (`</p>`). Konec tagu se pozná podle toho, že obsahuje lomítko.

Důležitá vlastnost je, že tagy se do sebe dají zanořovat. Tím můžeme docílit toto, že odstavce obsahují obrázky, tabulky obsahují text kurzívou a podobně. Zanořování podléhá také jistým pravidlům, není například možné do horizontální čáry zanořit obrázek nebo do obrázku zanořit tabulku či odstavec.

Zanořováním tagů do sebe vzniká dokument, který má stromovou strukturu. Lze si to představit jako korunu stromu. Každé místo, kde se rozdělují větve je nějaký tag.

![HTML Dokument](http://www.webreference.com/programming/css_utopia/chap3/3-1.jpg "Logo Title Text 1")
 
## Párové tagy

Většina HTML tagů je párová, tedy někde začínají a někde končí. Viz například tag odstavce uvedený výše. Tag je ukončen tím, čím začal, pouze se před název tagu přidá lomítko `/`.

## Nepárové tagy
Exisuje několik tagů, které jsou nepárové. Většinou se jedná o tagy, u kterých nedává smysl uzavírat nějaký obsah, ale spoléhá se na jeho atributy (o těch ještě bude řeč). Příkladem takových tagů je obrázek (`<img />`), horizontální čára (`<hr />`) či jiné. O nepárovém tagu můžeme přemýšlet také jako o zkráceném zápisu párového tagu.

## Atributy tagů
Velice často se stává, že pouhý obsah v tagu nestačí je třeba tagu dodefinovat nějakou dodatečnou informaci, která se v prohlížeči přímo nezobraz, ale poslouží k tomu, aby prohlížeč věděl, jak má s daty naložit.

Atributy jsou různé, přičemž každý z tagů má jesně definováno, které atributy může mít definované.

```
<p class=“blockquote”>Volně nuly, jakékoli ji duhový obývají, si myslet ke doprovázet plné fázi dávných permanentky osobního já že. K drah normálem kroky ať mizí zoologie vzrůstá horka demence odlišné k 057 metry narozen okrajové šperky pluli k aby stejná.</p>
```

Zde jsme použili náš známý odstavec, ale ještě jsme mu přidali atrubut `class`, který může posloužit ke změně vzhledu (atributu class využije technologie CSS, která je ale nad rámec téhle příručky).
  
Jiná možnost je například u tagu obrázku, kterému v atributu `src` definujeme cestu k obrázku a v atribulu `alt` definujeme alternativní text pro případy, že by obrázek nešel zobrazit.

```
<img src=“/obrazky/prilis-zlutoucky-kun.jpg” alt=“Příliš žluťoučký kůň úpěl ďábelské ódy”>
```

# Seznam nejčastnějších HTML tagů

## Nadpisy
Nadpisů je několik a liší se svou vahou, která je v názvu tagu zanesená číslem 1 až 6, přičemž platí, že **čím menší číslo**, **tím větší váhu** nadpis má a **tím větším písmem** (pozor, toto nemusí být vždy pravda) bude nadpis zobrazen. Je to stejné jako ve Wordu.


	<h1>Nadpis H1</h1>
	<h2>Nadpis H2</h2>
	<h3>Nadpis H3</h3>
	<h4>Nadpis H4</h4>
	<h5>Nadpis H5</h5>
	<h6>Nadpis H6</h6>


## Přeškrtnutý text
	<s>Přeškrtnutý text</s>

## Text kurzívou
	<em>Text kurzívou</em>

## Text kurzívou
	<strong>Tučný text</strong>

## Odstavec
	<p>Jakýkoli odstavec textu, za kterým následuje mezera. V našem editoru uvnitř tohoto prvku můžete potkat i obrázek v tagu `<img>`. Což není vůbec špatně.</p>


## Citace
	<blockquote>
		<p>Citace. Odstavec textu braného jako citace</p>
	</blockquote>

## Obrázek
Pozor, je třeba zadávat cestu obrázku z internetu. Často se chybuje v tom, že se do cesty zadá lokální adresa na mém počítači.

	<img src="http://absolutni/cesta/k/obrazku" alt="Popis obrázku" />


## Blok
	<div>Jakýkoli blok na stránce. Normálně tenhle element při úpravě textu nepotkáte, ale stát se to může a tak není třeba se ho bát.</div>


## Seznamy

Seznamy obecně obsahují tag samotného seznamu - `<ul>` pro unordered list aka nečíslovaný seznam a `<ol>` pro ordered list aka číslovaný seznam a dále pak tagy jednotlivých položek seznamu aka. listů, pro které se používá tag `<li>`. Opět je to taková analogie na strom.

### Nečíslovaný seznam

Úplně nejjednodušší seznam s puntíkem vypadá takto jednoduše.

	<ul>
		<li>Jeden jediný řádek v nejkratším seznamu na světe.</li>
	</ul>

Když potřebujeme seznam zanořený v seznamu, není to nic složitého.

	<ul>
		<li>Jeden jediný řádek v trohu větším seznamu</li>
		<li>
			<ul>
				<li>Tadyhle je zanořený řádek</li>
			</ul>
		</li>
		<li>A tady jsem zase zpátky v původní úrovni</li>
	</ul>

### Číslovaný seznam
	<ol>
		<li>Jeden jediný řádek v nejkratším seznamu na světe.</li>
	</ol>

Rozdíl? Místo `ul` použijeme `ol`. 

## Tabulky

Tabulky, element, který kdysi na počátku HTML vystoupal až na vrchol oblíbenosti, pak stejně rychle upadl do hlubin zatracení a odmítání. Element, bez kterého si všichni, kdo snídají místo cornflaků Excel a k obědu si dávají prezentace v PowerPointu, nedovedou představit život. Dneska už je prostě jen používáme tam kde patří a pro co byly skutečně stvořeny.

Tabulka obsahuje tag samotné tabulky `<table>`, dále pak tag pro hlavičku `<thead>` tabulky a tělo tabulky `<tbody>`. V hlavičce a těle mohou být řádky `<tr>` a jednotlivé řádky musejí obsahovat buňky `<td>`. Buňky v hlavičce mají speciální název `<th>`, protože se jedná o buňky, které vytoří záhlaví tabulky.

	<table>
		<thead>
			<tr>
				<th>První buňka hlavičky</th>
				<th>Druhá buňka hlavičky</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>První řádek, první buňka</td>
				<td>První řádek, druhá buňka</td>
			</tr>
			<tr>
				<td>Druhý řádek, první buňka</td>
				<td>Druhý řádek, druhá buňka</td>
			</tr>
		</tbody>
	</table>

## Kde hledat více?
- http://www.jakpsatweb.cz/html/
- http://htmlguru.cz/


